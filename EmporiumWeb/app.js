'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _https = require('https');

var _https2 = _interopRequireDefault(_https);

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
* IMPORTANT
* Change these paths to your own SSL and Apple Pay certificates,
* with the appropriate merchant identifier and domain
* See the README for more information.
*/
var APPLE_PAY_CERTIFICATE_PATH = "./certificates/apple_pay.pem"; /*
                                                                 Copyright (C) 2016 Apple Inc. All Rights Reserved.
                                                                 See LICENSE.txt for this sample’s licensing information
                                                                  
                                                                 Abstract:
                                                                 Sets up a simple Express HTTP server to host the example page, and handles requesting 
                                                                 the Apple Pay merchant session from Apple's servers.
                                                                 */

var SSL_CERTIFICATE_PATH = "./certificates/atanashristov.com.crt";
var SSL_KEY_PATH = "./certificates/atanashristov.com.key";
var MERCHANT_IDENTIFIER = "merchant.com.atanashristov";
var MERCHANT_DOMAIN = "atanashristov.com";

try {
	_fs2.default.accessSync(APPLE_PAY_CERTIFICATE_PATH);
	_fs2.default.accessSync(SSL_CERTIFICATE_PATH);
	_fs2.default.accessSync(SSL_KEY_PATH);
} catch (e) {
	throw new Error('You must generate your SSL and Apple Pay certificates before running this example.');
}

var sslKey = _fs2.default.readFileSync(SSL_KEY_PATH);
var sslCert = _fs2.default.readFileSync(SSL_CERTIFICATE_PATH);
var applePayCert = _fs2.default.readFileSync(APPLE_PAY_CERTIFICATE_PATH);

/**
* Set up our server and static page hosting
*/
var app = (0, _express2.default)();
app.use(_express2.default.static('public'));
app.use(_bodyParser2.default.json());

/**
* A POST endpoint to obtain a merchant session for Apple Pay.
* The client provides the URL to call in its body.
* Merchant validation is always carried out server side rather than on
* the client for security reasons.
*/
app.post('/getApplePaySession', function (req, res) {

	// We need a URL from the client to call
	if (!req.body.url) return res.sendStatus(400);

	// We must provide our Apple Pay certificate, merchant ID, domain name, and display name
	var options = {
		url: req.body.url,
		cert: applePayCert,
		key: applePayCert,
		method: 'post',
		body: {
			merchantIdentifier: MERCHANT_IDENTIFIER,
			domainName: MERCHANT_DOMAIN,
			displayName: 'My Store'
		},
		json: true
	};

	// Send the request to the Apple Pay server and return the response to the client
	(0, _request2.default)(options, function (err, response, body) {
		if (err) {
			console.log('Error generating Apple Pay session!');
			console.log(err, response, body);
			res.status(500).send(body);
		}
		res.send(body);
	});
});

/**
* Start serving the app.
*/
_https2.default.createServer({
	key: sslKey,
	cert: sslCert
}, app).listen(443);

